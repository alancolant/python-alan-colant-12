# Requirements

- Python 3.9.x

# Usage

## With venv

```sh
python3 -m venv /path/to/new/virtual/environment #Create venv
source /path/to/new/virtual/environment/bin/activate 

pip install -r requirements.txt

cd epic_events

python manage.py migrate
python manage.py runserver
```

## Without venv

```sh
pip install -r requirements.txt
cd epic_events
python manage.py migrate
python manage.py runserver
```

# Development

## Testing (behave)

### Firstly, you need to create superuser

```shell
cd epic_events
python manage.py createsuperuser
```

### Then, create 3 specific user for testing

**Open localhost:8000/admin**

* Admin:
    * username: test-admin
    * password: RandomPassword
    * role: ADMIN
* Sale:
    * username: test-sale
    * password: RandomPassword
    * role: SALE
* Support:
    * username: test-support
    * password: RandomPassword
    * role: SUPPORT

### Then, you need to create .env in test folder

```dotenv
API_URL=http://127.0.0.1:8000

API_ADMIN_USERNAME=test-admin
API_ADMIN_PASSWORD=RandomPassword

API_SALE_USERNAME=test-sale
API_SALE_PASSWORD=RandomPassword

API_SUPPORT_USERNAME=test-support
API_SUPPORT_PASSWORD=RandomPassword
```

```bash
cd epic_events
behave
```

## Interface

* http://127.0.0.1:8000/

# Postgresql

**You can start PostgreSQL with docker using this command:**

```shell
docker-compose up -d
```

**PgAdmin will be accessible at localhost:8014**

# Logs

**You can find logs in epic_events/django.log**