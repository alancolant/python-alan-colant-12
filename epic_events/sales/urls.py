from rest_framework.routers import flatten, SimpleRouter

from sales.views import ClientViewSet, ContractViewSet

clientsRouter = SimpleRouter()
clientsRouter.register(r'api/clients', ClientViewSet, basename="clients")
contractsRouter = SimpleRouter()
contractsRouter.register(r'api/contracts', ContractViewSet, basename="contracts")

urlpatterns = list(flatten([
    clientsRouter.urls,
    contractsRouter.urls,
]))
