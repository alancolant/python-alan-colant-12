from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.request import Request


class ClientPermission(BasePermission):
    def __init__(self):
        super().__init__()

    def __call__(self, *args, **kwargs):
        return self

    def has_object_permission(self, request: Request, view, client):
        if request.user.role == 'ADMIN':
            return True

        if request.method in SAFE_METHODS:
            return True

        return {
            'POST': True,
            'PUT': True,
            'PATCH': True,
            'DELETE': True,
        }[request.method]

    def has_permission(self, request: Request, view):
        return request.user.role in ['ADMIN', 'SALE']


class ContractPermission(BasePermission):
    def __init__(self):
        super().__init__()

    def __call__(self, *args, **kwargs):
        return self

    def has_object_permission(self, request: Request, view, contract):
        if request.user.role == 'ADMIN':
            return True

        if request.method in SAFE_METHODS:
            return True

        return {
            'POST': True,
            'PUT': contract.status != 'SIGNED',
            'PATCH': contract.status != 'SIGNED',
            'DELETE': contract.status != 'SIGNED',
        }[request.method]

    def has_permission(self, request: Request, view):
        return request.user.role in ['ADMIN', 'SALE']
