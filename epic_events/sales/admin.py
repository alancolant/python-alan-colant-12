from django.contrib import admin

from sales.models import Client, Contract

# Register your models here.
admin.site.register(Client)
admin.site.register(Contract)
