from django.db import models

from epic_events import settings


class Client(models.Model):
    role = models.CharField(
        max_length=8,
        choices=(('CLIENT', 'CLIENT'), ('PROSPECT', 'PROSPECT')),
        db_index=True,
        default='PROSPECT',
    )
    prospecting_status = models.CharField(
        max_length=16,
        choices=(('WAIT_COORDINATOR', 'WAIT_COORDINATOR'), ('IN_PROGRESS', 'IN_PROGRESS'), ('FINISHED', 'FINISHED')),
        db_index=True,
        default='WAIT_COORDINATOR',
    )
    coordinator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    email = models.EmailField(max_length=320, null=True, blank=True)
    phone = models.CharField(max_length=16, null=True, blank=True)
    mobile = models.CharField(max_length=16, null=True, blank=True)
    company_name = models.CharField(max_length=256, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "clients"


class Contract(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        db_index=True
    )
    status = models.CharField(
        max_length=16,
        choices=(('DRAFT', 'DRAFT'), ('WAIT_SIGN', 'WAIT_SIGN'), ('SIGNED', 'SIGNED')),
        db_index=True,
        default='DRAFT'
    )
    service_name = models.TextField(null=True, blank=True)
    amount = models.DecimalField(
        null=True,
        blank=True,
        max_digits=10,
        decimal_places=2
    )
    signed_at = models.DateTimeField(null=True, blank=True)
    payment_due = models.DateTimeField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "contracts"
