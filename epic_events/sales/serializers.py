from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField

from sales.models import Client, Contract
from support.models import Event


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'role', 'prospecting_status', 'coordinator_id', 'coordinator', 'first_name', 'last_name',
                  'email', 'phone', 'mobile', 'company_name']


class ContractSerializer(ModelSerializer):
    client__infos = ClientSerializer(many=False, read_only=True, source='client')
    client = PrimaryKeyRelatedField(queryset=Client.objects.filter(role="CLIENT"))

    class Meta:
        model = Contract
        fields = ['id', 'client', 'client__infos', 'status', 'service_name', 'amount', 'signed_at', 'payment_due']

    def save(self, **kwargs):
        super().save(**kwargs)
        if self.instance.status == 'SIGNED' and not hasattr(self.instance, 'event'):
            Event.objects.create(contract=self.instance, client=self.instance.client)
            return

        super().save(**kwargs)
