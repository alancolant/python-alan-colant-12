import django_filters
from django.db.models import Q
from django_filters import rest_framework as filters

from .models import Client, Contract


class ClientFilter(django_filters.FilterSet):
    email = filters.CharFilter(field_name="email", lookup_expr='contains')
    name = filters.CharFilter(label="Name contains", method='client_name_filter')

    def client_name_filter(self, queryset, name, value):
        return queryset.filter(
            Q(first_name__icontains=value) |
            Q(last_name__icontains=value)
        )

    class Meta:
        model = Client
        fields = ['prospecting_status', 'role']


class ContractFilter(django_filters.FilterSet):
    email = filters.CharFilter(field_name="client__email", lookup_expr='contains')
    name = filters.CharFilter(label="Client name contains", method='client_name_filter')

    min_date = filters.DateFilter(field_name="signed_at", lookup_expr='gte')
    max_date = filters.DateFilter(field_name="signed_at", lookup_expr='lte')

    min_amount = filters.NumberFilter(field_name="amount", lookup_expr='gte')
    max_amount = filters.NumberFilter(field_name="amount", lookup_expr='lte')

    def client_name_filter(self, queryset, name, value):
        return queryset.filter(
            Q(client__first_name__icontains=value) |
            Q(client__last_name__icontains=value)
        )

    class Meta:
        model = Contract
        fields = ['status', 'client']
