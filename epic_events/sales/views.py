from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from sales.models import Client, Contract
from sales.permissions import ContractPermission, ClientPermission
from sales.serializers import ClientSerializer, ContractSerializer
from .filters import ClientFilter, ContractFilter


class ClientViewSet(ModelViewSet):
    serializer_class = ClientSerializer
    permission_classes = [IsAuthenticated, ClientPermission]
    filterset_class = ClientFilter

    def get_queryset(self):
        return Client.objects.all()


class ContractViewSet(ModelViewSet):
    serializer_class = ContractSerializer
    permission_classes = [IsAuthenticated, ContractPermission]
    filterset_class = ContractFilter

    def get_queryset(self):
        return Contract.objects.all()
