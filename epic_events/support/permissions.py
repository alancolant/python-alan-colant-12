from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.request import Request


class EventPermission(BasePermission):
    def __init__(self):
        super().__init__()

    def __call__(self, *args, **kwargs):
        return self

    def has_object_permission(self, request: Request, view, event):
        if request.user.role == 'ADMIN':
            return True

        if request.method in SAFE_METHODS:
            return True

        return {
            'POST': True,
            'PUT': event.status != 'FINISHED',
            'PATCH': event.status != 'FINISHED',
            'DELETE': False,
        }[request.method]

    def has_permission(self, request: Request, view):
        if request.method == 'POST':
            return request.user.role == 'ADMIN'

        return request.user.role in ['ADMIN', 'SUPPORT']
