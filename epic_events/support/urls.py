from rest_framework.routers import flatten, SimpleRouter

from support.views import EventViewSet

eventRouter = SimpleRouter()
eventRouter.register(r'api/events', EventViewSet, basename="events")

urlpatterns = list(flatten([
    eventRouter.urls,
]))
