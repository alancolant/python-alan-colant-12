from django.db import models

from epic_events import settings
from sales.models import Client, Contract


class Event(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        db_index=True,
        related_name='events'
    )
    contract = models.OneToOneField(
        Contract,
        on_delete=models.CASCADE,
        db_index=True,
        related_name='event',
    )
    coordinator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        db_index=True,
        null=True,
        blank=True,
        related_name='events'
    )
    status = models.CharField(
        max_length=16,
        choices=(('WAIT_ASSIGN', 'WAIT_ASSIGN'), ('IN_PROGRESS', 'IN_PROGRESS'), ('FINISHED', 'FINISHED')),
        db_index=True,
        default='WAIT_ASSIGN'
    )
    commentary = models.TextField(null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "events"
