from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from support.models import Event
from support.permissions import EventPermission
from support.serializers import EventSerializer
from .filters import EventFilter


# Create your views here.


class EventViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated, EventPermission]
    serializer_class = EventSerializer
    filterset_class = EventFilter

    def get_queryset(self):
        query = Event.objects
        if self.request.user.role != 'ADMIN':
            query = query.filter(coordinator=self.request.user)
        return query.prefetch_related('client')
