from rest_framework.serializers import ModelSerializer

from sales.serializers import ClientSerializer
from support.models import Event


class EventSerializer(ModelSerializer):
    client__infos = ClientSerializer(many=False, read_only=True, source='client')

    class Meta:
        model = Event
        fields = ('id', 'client', 'contract', 'client__infos', 'date', 'commentary', 'status', 'coordinator')

    def get_fields(self):
        fields = super().get_fields()
        request = self.context.get('request')
        if request is not None and request.user.role != 'ADMIN':
            fields['client'].read_only = True
            fields.pop('coordinator', None)
        return fields
