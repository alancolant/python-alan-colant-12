import django_filters
from django.db.models import Q
from django_filters import rest_framework as filters

from .models import Event


class EventFilter(django_filters.FilterSet):
    client_email = filters.CharFilter(field_name="client__email", lookup_expr='contains')
    client_name = filters.CharFilter(label="Client name contains", method='client_name_filter')
    min_date = filters.DateFilter(field_name="date", lookup_expr='gte')
    max_date = filters.DateFilter(field_name="date", lookup_expr='lte')

    def client_name_filter(self, queryset, name, value):
        return queryset.filter(
            Q(client__first_name__icontains=value) |
            Q(client__last_name__icontains=value)
        )

    class Meta:
        model = Event
        fields = ['status', 'client', 'contract']
