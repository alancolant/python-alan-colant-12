from django.contrib import admin

from support.models import Event

# Register your models here.
admin.site.register(Event)
