Feature: API - Clients (Errors)

  Scenario: Prepare clients error tests
    Given set base_url "{env:API_URL}"

  Scenario: Errors are returned when no datas
    Given ensure bearer_token is set for "{env:API_ADMIN_USERNAME}" with password "{env:API_ADMIN_PASSWORD}"
    When do "POST" request to "/api/clients/"
    Then response code is "400"
    And response has JSON structure
      | Field        | Value                   |
      | first_name.0 | This field is required. |
      | last_name.0  | This field is required. |

  Scenario: Support cannot access clients
    Given ensure bearer_token is set for "{env:API_SUPPORT_USERNAME}" with password "{env:API_SUPPORT_PASSWORD}"
    When do "POST" request to "/api/clients/"
    Then response code is "403"

  Scenario: Cannot access endpoint when user is not logged in
    Given ensure bearer_token is not set
    When do "POST" request to "/api/clients/"
    Then response code is "401"