Feature: API - Events (Errors)

  Scenario: Prepare events errors tests
    Given set base_url "{env:API_URL}"

  Scenario: Errors are returned when no datas
    Given ensure bearer_token is set for "{env:API_ADMIN_USERNAME}" with password "{env:API_ADMIN_PASSWORD}"
    When do "POST" request to "/api/events/"
    Then response code is "400"
    And response has JSON structure
      | Field      | Value                   |
      | contract.0 | This field is required. |
      | client.0   | This field is required. |
    
  Scenario: Sales cannot access events
    Given ensure bearer_token is set for "{env:API_SALE_USERNAME}" with password "{env:API_SALE_PASSWORD}"
    When do "POST" request to "/api/events/"
    Then response code is "403"

  Scenario: Cannot access endpoint when user is not logged in
    Given ensure bearer_token is not set
    When do "POST" request to "/api/events/"
    Then response code is "401"