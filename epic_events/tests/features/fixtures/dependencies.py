import re

from behave import fixture
from behave.model_core import Status


@fixture
def before_all(context):
    context.status = {}


@fixture
def before_scenario(context, scenario):
    depend_names = []

    for tag in scenario.effective_tags:
        matchs = re.findall(r"dependsOnPassed\('(.*?)'\)", tag)
        if len(matchs) > 0:
            depend_names.append(*matchs)

    for depend_name in depend_names:
        if context.status[scenario.feature.name][depend_name] != Status.passed:
            scenario.skip(f'Dependency {depend_name} is not marked as passed')
            break


@fixture
def after_scenario(context, scenario):
    depend_names = []

    for tag in scenario.effective_tags:
        matchs = re.findall(r"depend\('(.*?)'\)", tag)
        if len(matchs) > 0:
            depend_names.append(*matchs)

    for depend_name in depend_names:
        if scenario.feature.name in context.status:
            context.status[scenario.feature.name][depend_name] = scenario.status
        else:
            context.status[scenario.feature.name] = {depend_name: scenario.status}
