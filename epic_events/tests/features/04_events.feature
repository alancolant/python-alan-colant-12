Feature: API - Events

  Scenario: Prepare events tests
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "{env:API_ADMIN_USERNAME}" with password "{env:API_ADMIN_PASSWORD}"
    #Create client
    When do "POST" request to "/api/clients/"
      | Field        | Value           |
      | first_name   | Test client     |
      | last_name    | Test client     |
      | email        | test@client.com |
      | phone        | 0606060606      |
      | mobile       | 0606060606      |
      | company_name | Test RS         |
      | role         | CLIENT          |

    #Assertions
    Then response code is "201"
    And response has JSON structure
      | Field | Value |
      | id    | N/A   |
    And add JSON value "id" in session as "client_id"

    #Create contract
    When do "POST" request to "/api/contracts/"
      | Field  | Value               |
      | client | {session:client_id} |

    #Assertions
    Then response code is "201"
    And response has JSON structure
      | Field | Value |
      | id    | N/A   |
    And add JSON value "id" in session as "contract_id"


  @depend('create')
  Scenario: Can create events
    #Action
    When do "POST" request to "/api/events/"
      | Field      | Value                 |
      | client     | {session:client_id}   |
      | contract   | {session:contract_id} |
      | status     | IN_PROGRESS           |
      | commentary | Commentaire           |
      | date       | 2022-01-01            |

    #Assertions
    Then response code is "201"
    And response has JSON structure
      | Field      | Value                 |
      | id         | N/A                   |
      | client     | {session:client_id}   |
      | contract   | {session:contract_id} |
      | status     | IN_PROGRESS           |
      | commentary | Commentaire           |
    And add JSON value "id" in session as "event_id"

  @dependsOnPassed('create')
  Scenario: Can get previously created event
    #Action
    When do "GET" request to "/api/events/{session:event_id}"

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field      | Value                 |
      | id         | {session:event_id}    |
      | client     | {session:client_id}   |
      | contract   | {session:contract_id} |
      | status     | IN_PROGRESS           |
      | commentary | Commentaire           |

  @dependsOnPassed('create')
  Scenario: Can update previously created event
    #Action
    When do "PUT" request to "/api/events/{session:event_id}/"
      | Field      | Value                 |
      | client     | {session:client_id}   |
      | contract   | {session:contract_id} |
      | status     | WAIT_ASSIGN           |
      | commentary | Commentaire modif     |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field      | Value                 |
      | id         | {session:event_id}    |
      | client     | {session:client_id}   |
      | contract   | {session:contract_id} |
      | status     | WAIT_ASSIGN           |
      | commentary | Commentaire modif     |


  @dependsOnPassed('create')
  Scenario: Can update partially previously created event
    #Action
    When do "PATCH" request to "/api/events/{session:event_id}/"
      | Field      | Value                    |
      | commentary | Commentaire encore modif |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field      | Value                    |
      | id         | {session:event_id}       |
      | commentary | Commentaire encore modif |

  @dependsOnPassed('create')
  Scenario: Can delete previously created event
    When do "DELETE" request to "/api/events/{session:event_id}/"
    Then response code is "204"
