Feature: API - Contracts

  Scenario: Prepare contracts tests
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "{env:API_ADMIN_USERNAME}" with password "{env:API_ADMIN_PASSWORD}"
    When do "POST" request to "/api/clients/"
      | Field        | Value           |
      | first_name   | Test client     |
      | last_name    | Test client     |
      | email        | test@client.com |
      | phone        | 0606060606      |
      | mobile       | 0606060606      |
      | company_name | Test RS         |
      | role         | CLIENT          |

    #Assertions
    Then response code is "201"
    And response has JSON structure
      | Field | Value |
      | id    | N/A   |
    And add JSON value "id" in session as "client_id"


  @depend('create')
  Scenario: Can create contract and get returned
    #Action
    When do "POST" request to "/api/contracts/"
      | Field  | Value               |
      | client | {session:client_id} |

    #Assertions
    Then response code is "201"
    And response has JSON structure
      | Field  | Value               |
      | id     | N/A                 |
      | client | {session:client_id} |
    And add JSON value "id" in session as "contract_id"

  @dependsOnPassed('create')
  Scenario: Can get previously created contract
    #Action
    When do "GET" request to "/api/contracts/{session:contract_id}"

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field  | Value                 |
      | id     | {session:contract_id} |
      | client | {session:client_id}   |
      | status | DRAFT                 |

  @dependsOnPassed('create')
  Scenario: Can update previously created contract
    #Action
    When do "PUT" request to "/api/contracts/{session:contract_id}/"
      | Field  | Value               |
      | client | {session:client_id} |
      | status | WAIT_SIGN           |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field  | Value                 |
      | id     | {session:contract_id} |
      | client | {session:client_id}   |
      | status | WAIT_SIGN             |


  @dependsOnPassed('create')
  Scenario: Can update partially previously created contract
    #Action
    When do "PATCH" request to "/api/contracts/{session:contract_id}/"
      | Field  | Value     |
      | status | WAIT_SIGN |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field  | Value                 |
      | id     | {session:contract_id} |
      | status | WAIT_SIGN             |

  @dependsOnPassed('create')
  Scenario: Event automatically created when status switch to SIGNED
    #Action
    When do "PATCH" request to "/api/contracts/{session:contract_id}/"
      | Field  | Value  |
      | status | SIGNED |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field  | Value                 |
      | id     | {session:contract_id} |
      | status | SIGNED                |

    When do "GET" request to "/api/events?contract={session:contract_id}"
    Then response code is "200"
    And response has JSON structure
      | Field      | Value                 |
      | 0.id       | N/A                   |
      | 0.contract | {session:contract_id} |


  @dependsOnPassed('create')
  Scenario: Can delete previously created contract
    When do "DELETE" request to "/api/contracts/{session:contract_id}/"
    Then response code is "204"
