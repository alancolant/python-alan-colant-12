Feature: API - Users (Errors)

  Scenario: Prepare users errors tests
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is not set


  Scenario: Signup return errors when incorrect data
    #Action
    When do "POST" request to "/api/signup"
    Then response code is "400"
    And response has JSON structure
      | Field        | Value |
      | username.0   | N/A   |
      | password.0   | N/A   |
      | password2.0  | N/A   |
      | email.0      | N/A   |
      | first_name.0 | N/A   |
      | last_name.0  | N/A   |
