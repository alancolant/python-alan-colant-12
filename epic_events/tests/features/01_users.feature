Feature: API - Users

  Scenario: Prepare users tests
    Given set base_url "{env:API_URL}"

  @depend('signup')
  Scenario: Can create user
    When do "POST" request to "/api/signup"
      | Field      | Value                 |
      | username   | test-{rand:12}        |
      | password   | @frgw<gsfdqg          |
      | password2  | @frgw<gsfdqg          |
      | email      | {rand:12}@example.com |
      | first_name | Tester                |
      | last_name  | Example               |

    Then response code is "201"
    And response has JSON structure
      | Field      | Value   |
      | username   | N/A     |
      | email      | N/A     |
      | first_name | Tester  |
      | last_name  | Example |
      | role       | SALE    |
    And add JSON value "username" in session as "username"

  @dependsOnPassed('signup')
  Scenario: Can login user
    #Action
    When do "POST" request to "/api/login"
      | Field    | Value              |
      | username | {session:username} |
      | password | @frgw<gsfdqg       |

    Then response code is "200"
    And response has JSON structure
      | Field   | Value |
      | access  | N/A   |
      | refresh | N/A   |
