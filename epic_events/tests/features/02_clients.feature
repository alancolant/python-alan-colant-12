Feature: API - Clients

  Scenario: Prepare clients tests
    Given set base_url "{env:API_URL}"
    Given ensure bearer_token is set for "{env:API_ADMIN_USERNAME}" with password "{env:API_ADMIN_PASSWORD}"

  @depend('create')
  Scenario: Can create project and get returned
    #Action
    When do "POST" request to "/api/clients/"
      | Field        | Value           |
      | first_name   | Test client     |
      | last_name    | Test client     |
      | email        | test@client.com |
      | phone        | 0606060606      |
      | mobile       | 0606060606      |
      | company_name | Test RS         |

    #Assertions
    Then response code is "201"
    And response has JSON structure
      | Field        | Value           |
      | id           | N/A             |
      | first_name   | Test client     |
      | last_name    | Test client     |
      | email        | test@client.com |
      | phone        | 0606060606      |
      | mobile       | 0606060606      |
      | company_name | Test RS         |
    And add JSON value "id" in session as "client_id"

  @dependsOnPassed('create')
  Scenario: Can get previously created client
    #Action
    When do "GET" request to "/api/clients/{session:client_id}"

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field        | Value               |
      | id           | {session:client_id} |
      | first_name   | Test client         |
      | last_name    | Test client         |
      | email        | test@client.com     |
      | phone        | 0606060606          |
      | mobile       | 0606060606          |
      | company_name | Test RS             |

  @dependsOnPassed('create')
  Scenario: Can update previously created project
    #Action
    When do "PUT" request to "/api/clients/{session:client_id}/"
      | Field        | Value               |
      | first_name   | mod-Test client     |
      | last_name    | mod-Test client     |
      | email        | mod-test@client.com |
      | phone        | mod-0606060606      |
      | mobile       | mod-0606060606      |
      | company_name | mod-Test RS         |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field        | Value               |
      | id           | {session:client_id} |
      | first_name   | mod-Test client     |
      | last_name    | mod-Test client     |
      | email        | mod-test@client.com |
      | phone        | mod-0606060606      |
      | mobile       | mod-0606060606      |
      | company_name | mod-Test RS         |


  @dependsOnPassed('create')
  Scenario: Can update partially previously created client
    #Action
    When do "PATCH" request to "/api/clients/{session:client_id}/"
      | Field | Value      |
      | phone | 0705040508 |

    #Assertions
    Then response code is "200"
    And response has JSON structure
      | Field        | Value               |
      | id           | {session:client_id} |
      | first_name   | mod-Test client     |
      | last_name    | mod-Test client     |
      | email        | mod-test@client.com |
      | phone        | 0705040508          |
      | mobile       | mod-0606060606      |
      | company_name | mod-Test RS         |

  @dependsOnPassed('create')
  Scenario: Can delete previously created client
    When do "DELETE" request to "/api/clients/{session:client_id}/"
    Then response code is "204"
