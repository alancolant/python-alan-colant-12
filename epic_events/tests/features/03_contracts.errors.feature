Feature: API - Contracts (Errors)

  Scenario: Prepare clients error tests
    Given set base_url "{env:API_URL}"

  Scenario: Cannot access endpoint when user is not logged in
    Given ensure bearer_token is not set
    When do "POST" request to "/api/contracts/"
    Then response code is "401"
    
  Scenario: Support cannot access clients
    Given ensure bearer_token is set for "{env:API_SUPPORT_USERNAME}" with password "{env:API_SUPPORT_PASSWORD}"
    When do "POST" request to "/api/contracts/"
    Then response code is "403"

  Scenario: Errors are returned when no datas
    Given ensure bearer_token is set for "{env:API_ADMIN_USERNAME}" with password "{env:API_ADMIN_PASSWORD}"
    When do "POST" request to "/api/contracts/"
    Then response code is "400"
    And response has JSON structure
      | Field    | Value                   |
      | client.0 | This field is required. |

  Scenario: Cannot create contract for "PROSPECT"
    Given ensure bearer_token is set for "{env:API_ADMIN_USERNAME}" with password "{env:API_ADMIN_PASSWORD}"

    #Create client as prospect
    When do "POST" request to "/api/clients/"
      | Field      | Value         |
      | first_name | Test prospect |
      | last_name  | Test prospect |
      | role       | PROSPECT      |
    Then response code is "201"
    And response has JSON structure
      | Field | Value |
      | id    | N/A   |
    And add JSON value "id" in session as "client_id"

    #Check error
    When do "POST" request to "/api/contracts/"
      | Field  | Value               |
      | client | {session:client_id} |
    Then response code is "400"
    And response has JSON key "client.0"