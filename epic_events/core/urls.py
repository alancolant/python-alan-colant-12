from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView

from core.views import RegisterView

urlpatterns = [
    path('api/login', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/signup', RegisterView.as_view(), name='register')
]
