from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    role = models.CharField(
        max_length=16,
        choices=(('ADMIN', 'ADMIN'), ('SALE', 'SALE'), ('SUPPORT', 'SUPPORT')),
        db_index=True,
        default='SALE'
    )
