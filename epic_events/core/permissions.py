from rest_framework.permissions import BasePermission
from rest_framework.request import Request


class UserHasAnyRole(BasePermission):
    def __init__(self, permissions: list[str]):
        super().__init__()
        self.permissions = permissions

    def __call__(self, *args, **kwargs):
        return self

    def has_permission(self, request: Request, view):
        return request.user.role in self.permissions
