# TABLES

* Users/Permissions/Groups

`DJANGO MANAGED`

* Prospects/Clients (Is splitting necessary?)

| column             | type                                        | index | nullable |     default      |
|--------------------|---------------------------------------------|:-----:|:--------:|:----------------:|
| id                 | UBIGINT                                     |   x   |          |       A/I        |
| role               | ENUM(CLIENT/PROSPECT)                       |   x   |          |     PROSPECT     |
| prospecting_status | ENUM(WAIT_COORDINATOR/IN_PROGRESS/FINISHED) |   x   |          | WAIT_COORDINATOR |
| coordinator_id     | UBIGINT - FK(users->id) DC-UC               |       |    x     |       null       |
| firstname          | VARCHAR(32)                                 |       |    ?     |                  |
| lastname           | VARCHAR(32)                                 |       |    ?     |                  |
| email              | VARCHAR(320)                                |       |    ?     |                  |
| phone              | VARCHAR(20)                                 |       |    ?     |                  |
| mobile             | VARCHAR(20)                                 |       |    ?     |                  |
| company_name       | VARCHAR(256)                                |       |    ?     |                  |
| created_at         | DATETIME                                    |       |          |       NOW        |
| updated_at         | DATETIME                                    |       |          |       NOW        |

* Contracts

| column       | type                            | index | nullable | default |
|--------------|---------------------------------|:-----:|:--------:|:-------:|
| id           | UBIGINT                         |   x   |          |   A/I   |
| client_id    | UBIGINT - FK(clients->id) DN-UC |   x   |    x     |         |
| status       | ENUM(DRAFT/WAIT_SIGN/SIGNED)    |   x   |          |  DRAFT  |
| service_name | TEXT                            |       |    x     |  null   |
| amount       | FLOAT(10,2)                     |       |    x     |  null   |
| signed_at    | DATETIME                        |       |    x     |  null   |
| payment_due  | DATETIME                        |       |    x     |  null   |
| created_at   | DATETIME                        |       |          |   NOW   |
| updated_at   | DATETIME                        |       |          |   NOW   |

* Events

| column         | type                                   | index | nullable |   default   |
|----------------|----------------------------------------|:-----:|:--------:|:-----------:|
| id             | UBIGINT - PK                           |   x   |          |     A/I     |
| client_id      | UBIGINT - FK(clients->id) DN-UC        |   x   |    x     |             |
| coordinator_id | UBIGINT - FK(users->id) DC-UC          |       |    x     |    null     |
| status         | ENUM(WAIT_ASSIGN/IN_PROGRESS/FINISHED) |   x   |          | WAIT_ASSIGN |
| commentary     | TEXT                                   |       |    x     |    null     |
| date           | DATETIME                               |       |    ?     |             |
| created_at     | DATETIME                               |       |          |     NOW     |
| updated_at     | DATETIME                               |       |          |     NOW     |